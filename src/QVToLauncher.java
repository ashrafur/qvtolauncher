import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
//import org.eclipse.ocl.pivot.internal.resource.ProjectMap;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;
import org.eclipse.m2m.qvt.oml.ExecutionContextImpl;
import org.eclipse.m2m.qvt.oml.ExecutionDiagnostic;
import org.eclipse.m2m.qvt.oml.ModelExtent;
import org.eclipse.m2m.qvt.oml.TransformationExecutor;
//import org.eclipse.m2m.qvt.oml.*;
import org.eclipse.ocl.examples.domain.utilities.ProjectMap;


public class QVToLauncher {
	public static void main( String[] args ) throws Exception{
		
	String inpMeta = "./metamodel/inpMeta.xmi";
	String inpModel = "./model/inpModel.xmi";
	String outMeta = "./metamodel/outMeta.xmi";
	String outModel = "./model/outModel.xmi";
	String tr = "./trans/transform.xmi";


	ExecutionContextImpl context= new ExecutionContextImpl();
	context.setConfigProperty("keepModeling", true);
	
	
	ResourceSet resourceSet = new ResourceSetImpl();
	Registry reg = Registry.INSTANCE; 

	resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("ecore", new EcoreResourceFactoryImpl());
	resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());

	ProjectMap.getAdapter(resourceSet);
	EPackage testA = (EPackage)(resourceSet.getResource(URI.createFileURI(inpMeta), true)).getContents().get(0);
	reg.put(testA.getNsURI(), testA);
	
	Resource  inResource = resourceSet.getResource(URI.createFileURI(inpModel),true);
	EPackage testB = (EPackage)(resourceSet.getResource(URI.createFileURI(outMeta), true)).getContents().get(0);
	reg.put(testB.getNsURI(), testB);
		
	EList<EObject> inObjects = inResource.getContents();
	ModelExtent input = new BasicModelExtent(inObjects);		
	ModelExtent output = new BasicModelExtent();

	//setup transformation
	URI transformationURI = URI.createFileURI(tr);
	//executor
	TransformationExecutor executor = new TransformationExecutor(transformationURI);
		

	ExecutionDiagnostic transformationResult = executor.execute(context, input, output);
	//output
	List<EObject> outObjects = output.getContents();

	//Debag Messages
	System.out.print(transformationResult.getMessage() + ". ");
	System.out.print(transformationURI.toString()+"\n");

	
	// create a new resource for saving 
	ResourceSet resourceSet2 = new ResourceSetImpl();
	
	
	resourceSet2.getResourceFactoryRegistry().getExtensionToFactoryMap().put("ecore", new EcoreResourceFactoryImpl());
	resourceSet2.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
	
	ProjectMap.getAdapter(resourceSet2);
		
	
	Resource outResource = resourceSet2.getResource(URI.createFileURI(outModel), true);
	outResource.getContents().clear();
	outResource.getContents().addAll(outObjects);
	
	
	try {
		outResource.save(Collections.EMPTY_MAP);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}
}
